/*
 * Clase encargada de la lógica del modelo (gestiona la persistencia)
 */
package model;

import entities.Cocinero;
import exceptions.RestaurantException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author mfontana
 */
@Stateless
public class RestaurantEJB {
    // Clase que nos dará los EntityManager
    // que necesitaremos para gestionar la persistencia
    @PersistenceUnit EntityManagerFactory emf;
    
    
    public void insertarCocinero(Cocinero c) throws RestaurantException {
       EntityManager em = emf.createEntityManager();
       // Comprobamos si existe ya un cocinero
       Cocinero auxiliar = em.find(Cocinero.class, c.getNombre());
       if (auxiliar != null) {
           em.close();
           throw new RestaurantException("Ya existe un cocinero con ese nombre");
       }
       // persist save en la bbdd
       em.persist(c);
       // close cierra el em y hace flush (vacía buffer)
       em.close();
    }
    
    public List<Cocinero> selectAllCocineros() {
        return emf.createEntityManager().createNamedQuery("Cocinero.findAll").getResultList();
    }
}
